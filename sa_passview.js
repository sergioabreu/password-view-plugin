/* 
 * Password viewer by Sergio Abreu
 * 06-12-2017
 * Updated: 04-06-2018
 * http://sites.sitesbr.net
 * Requires glyphicons css fonts  
 */

var SA_PassView = {
   langs: ['pt-br', 'en'],
   lang: 0,
   wClick: ['Clique para', 'Click to'],
   wPass: ['a senha', 'the password'],
   wShow: ['mostrar', 'show'],
   wHide: ['esconder', 'hide'],
   add: function(obj){      
     var clas = SA_PassView;
     var sp = document.createElement('span'), action=clas.wShow[ clas.lang];
     if(! obj.iconTarget){
       obj.iconTarget = obj.domElem.parentNode;
       obj.style = 'margin-left: 10px';
     }
     if( obj.style) sp.setAttribute("style", obj.style);
     sp.style.fontSize = "1.1em";
     sp.setAttribute('class', "glyphicon glyphicon-eye-close");
     sp.setAttribute("title", clas.wClick[ clas.lang] + " "+action+" " + clas.wPass[ clas.lang]);
     sp.addEventListener('click', function(e){
       var clas = SA_PassView;
       if(!e) e = window.event;
       var cl = e.target.getAttribute('class');
       if(cl.match(/close/)) {
         action = clas.wHide[ clas.lang];
         sp.setAttribute('class', "glyphicon glyphicon-eye-open");
         obj.domElem.type = "text";
       } else {
         action = clas.wShow[ clas.lang];
         sp.setAttribute('class', "glyphicon glyphicon-eye-close");
         obj.domElem.type = "password";
       }
       sp.setAttribute("title", clas.wClick[ clas.lang] + " "+action+" " + clas.wPass[ clas.lang]);
     });
     obj.iconTarget.appendChild(sp);
   },
   /* Basic obj is:
    * { 
    *   domElem: (the form field)
    * }
    *  
    * If you want to place the eye in a specific DOM element, use iconTarget property.
    * 
    *    iconTarget: (dom element to place the icon)
    *  
    * If ommited iconTarget, the domElem.parentNode will be used and margin-left is added automatically.
    *  
    * Accepts also a style property, holding the string for initial style for the eye: 
    *   style: 'margin-left: 10px' 
    */
  
};